<?php

namespace App\Classe;

use Mailjet\Client;
use Mailjet\Resources;

class Mail
{

    private string $apiKeyMailJet;
    private string $apiKeyMailJetSecret;

    /**
     * @param string $apiKeyStripe
     */
    public function __construct(string $apiKeyMailJet,string $apiKeyMailJetSecret)
    {
        $this->apiKeyMailJet = $apiKeyMailJet;
        $this->apiKeyMailJetsecret = $apiKeyMailJetSecret;
    }
    

    public function send($to_email, $to_name, $subject, $content)
    {
        $mj = new Client($this->apiKeyMailJet, $this->apiKeyMailJetSecret,true,['version' => 'v3.1']);
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => "mlejeunepro@gmail.com",
                        'Name' => "Maxime LE JEUNE - Developpeur"
                    ],
                    'To' => [
                        [
                            'Email' => $to_email,
                            'Name' => $to_name
                        ]
                    ],
                    'TemplateID' => 3102296,
                    'TemplateLanguage' => true,
                    'Subject' => $subject,
                    'Variables' => [
                        'content' => $content,
                    ]
                ]
            ]
        ];
        $response = $mj->post(Resources::$Email, ['body' => $body]);
        $response->success();
    }
}