<?php

namespace App\Controller;

use App\Entity\Curriculum;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager){
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="home")
     */
    public function index(): Response
    {
        return $this->render('home/index.html.twig');
    }

    /**
     * @Route("/a-propos", name="a-propos")
     */
    public function about(): Response
    {
        return $this->render('home/about.html.twig');
    }

    /**
     * @Route("/experiences", name="experiences")
     */
    public function experiences(): Response
    {
        return $this->render('home/experiences.html.twig');
    }

    /**
     * @Route("/competences", name="competences")
     */
    public function competences(): Response
    {
        return $this->render('home/competences.html.twig');
    }

    /**
     * @Route("/curriculum-vitae", name="cv")
     */
    public function curriculum(): Response
    {
        $curriculums = $this->entityManager->getRepository(Curriculum::class)->findAll();
        return $this->render('home/cv.html.twig', [
            'curriculums' => $curriculums
        ]);
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contact(): Response
    {
        return $this->render('home/contact.html.twig');
    }

    /**
     * @Route("/stages", name="stages")
     */
    public function stages(): Response
    {
        return $this->render('home/stages.html.twig');
    }

    /**
     * @Route("/mes-projets", name="projets")
     */
    public function projets(): Response
    {
        return $this->render('home/projets.html.twig');
    }

    /**
     * @Route("/mes-liens-utiles", name="liens")
     */
    public function links(): Response
    {
        return $this->render('home/liens.html.twig');
    }

    /**
     * @Route("/sortie", name="sortie")
     */
    public function exit(): Response
    {
        return $this->render('home/sortie.html.twig');
    }
}
