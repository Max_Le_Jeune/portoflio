<?php

namespace App\Controller;

use App\Classe\Mail;
use App\Entity\Recommendation;
use App\Form\RecommendationType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RecommendationController extends AbstractController
{

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager){
        $this->entityManager = $entityManager;
    }
    /**
     * @Route("/recommendation", name="recommendation")
     */
    public function index(Request $request): Response
    {
        $recommandations = $this->entityManager->getRepository(Recommendation::class)->findByState(1);
        $recommendation = new Recommendation();

        $form = $this->createForm(RecommendationType::class, $recommendation);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $recommendation->setState(0);
            $form->getData();
            $recommendation = $form->getData();
             $this->entityManager->persist($recommendation);
             $this->entityManager->flush();

            $this->addFlash('notice','Merci de nous avoir contacter. Notre equipe va vous répondre dans les meilleurs delais');
            $mail = new Mail();
            $mail->send('mlejeunepro@gmail.com', 'INFO', 'Nouvelle recommandation de '. $recommendation->getName(). ' '.$recommendation->getlastname(), 'Maxime '. $recommendation->getName(). ' '.$recommendation->getlastname(). ' ta laisse une recommandation consulte la depuis ton back office');
        }

        return $this->render('recommendation/index.html.twig', [
            'form' => $form->createView(),
            'recommandations'=>$recommandations
        ]);
    }
}
