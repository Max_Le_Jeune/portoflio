<?php

namespace App\Form;

use App\Entity\Recommendation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RecommendationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class, [
                'label'=> 'Entrez votre prenom',
                'required'=> true,
                'attr'=> [
                    'placeholder'=> 'Saisir votre prenom'
                ]
                ])
            ->add('lastname',TextType::class, [
                'label'=> 'Entrez votre nom',
                'required'=> true,
                'attr'=> [
                    'placeholder'=> 'Saisir votre nom'
                ]
            ])
            ->add('email',EmailType::class, [
                'label'=> 'Saissisez votre email',
                'required'=> true,
                'attr'=> [
                    'placeholder'=> 'Saisir votre email'
                ]
            ])
            ->add('compagny',TextType::class, [
                'label'=> 'Entrez votre societe',
                'required'=> true,
                'attr'=> [
                    'placeholder'=> 'Saisir votre nom de societe'
                ]
            ])
            ->add('message',TextareaType::class, [
                'label'=> 'Dites moi tout',
                'required'=> true,
                'attr'=> [
                    'placeholder'=> 'Saisir votre message'
                ]
            ])
            ->add('submit',SubmitType::class, [
                'label'=> 'Envoyer',
                'attr'=> [
                    'class'=> 'form-style',
                    'type'=> 'submit'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Recommendation::class,
        ]);
    }
}
